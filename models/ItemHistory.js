const mongoose = require('mongoose');
const { Schema } = mongoose;

const itemHistorySchema = new Schema({
	name: String,
	quantity: Number,
	price: Number,
	date: Date
});

module.exports = itemHistorySchema;