const mongoose = require('mongoose');
const { Schema } = mongoose;
const ItemHistorySchema = require('./ItemHistory');

const itemSchema = new Schema({
	name: String,
	quantity: Number,
	price: Number, //In cents
	dateCreated: Date,
	dateUpdated: Date,
	history: [ItemHistorySchema]
});

mongoose.model('items', itemSchema);
