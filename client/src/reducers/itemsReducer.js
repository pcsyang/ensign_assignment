import { 
	CREATE_ITEM,
	FETCH_ITEMS,
	FETCH_SINGLE_ITEM,
	UPDATE_ITEM,
	DELETE_ITEM 
} from '../actions/types';

export default (state={}, action) => {
	switch (action.type) {
		case FETCH_ITEMS:
			return { 
				...state, 
				...action.payload.reduce((newState, item) => {
					newState[item._id] = item;
					return newState;
				}, {})
			};
		case FETCH_SINGLE_ITEM:
			return { ...state, [action.payload._id]: action.payload };
		case CREATE_ITEM:
			return { ...state, [action.payload._id]: action.payload };
		case UPDATE_ITEM:
			return { ...state, [action.payload._id]: action.payload };
		case DELETE_ITEM:
			const {[action.payload._id]: omitted, ...rest} = state;
			return rest;
		default:
			return state;
	}
}





// export default function (state = [], action) {
// 	// console.log(action);

// 	switch (action.type) {
// 		case FETCH_ITEMS:
// 			return action.payload;
// 		default:
// 			return state;
// 	}
// }

