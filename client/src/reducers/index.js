import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import itemsReducer from './itemsReducer';

export default combineReducers({
	items: itemsReducer,
	form: reduxForm,
});

//So when we export the above combineReducers, we basically can access this.props.VARIABLE_NAME at the react side.