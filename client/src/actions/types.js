export const CREATE_ITEM = 'create_item';
export const FETCH_ITEMS = 'fetch_items';
export const FETCH_SINGLE_ITEM = 'fetch_single_item';
export const UPDATE_ITEM = 'update_item';
export const DELETE_ITEM = 'delete_item';

