import axios from 'axios';
import history from '../history';
import { 
	CREATE_ITEM,
	FETCH_ITEMS,
	FETCH_SINGLE_ITEM,
	UPDATE_ITEM,
	DELETE_ITEM 
} from './types';

//Action creators
export const createItem = (values) => async (dispatch) => {
	const res = await axios.post('/api/item/create', values);

	dispatch({ type: CREATE_ITEM, payload: res.data });
	history.push('/');
};

export const fetchItems = () => async (dispatch) => {
	const res = await axios.get('/api/items');

	dispatch({ type: FETCH_ITEMS, payload: res.data });
};

export const fetchSingleItem = (id) => async (dispatch) => {
	const res = await axios.get(`/api/item/${id}`);

	dispatch({ type: FETCH_SINGLE_ITEM, payload: res.data });
};

export const updateItem = (id, values) => async (dispatch) => {
	// console.log("updateItem: ", id, values);
	const res = await axios.patch(`/api/item/${id}`, values);

	dispatch({ type: UPDATE_ITEM, payload: res.data });
	history.push('/');
};

export const deleteItem = (id) => async (dispatch) => {
	const res = await axios.delete(`/api/item/${id}`, id);

	dispatch({ type: DELETE_ITEM, payload: res.data });
	history.push('/');
};
