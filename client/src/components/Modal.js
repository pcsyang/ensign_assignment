import React from 'react';
import ReactDOM from 'react-dom';

const Modal = (props) => {
	return ReactDOM.createPortal(
		<div 
			onClick={props.onDismiss} 
			style={{"marginTop":"-85px", width:"100vw", height:"100vh", "backgroundColor":"grey"}}>
			<div 
				onClick={(e) => e.stopPropagation()}
				className="modal open" style={{display:"block", "marginTop":"20%"}}>
				<div className="modal-content">
					<h5>{props.title}</h5>
					{props.content}
				</div>
				<div className="modal-footer">{props.actions}</div>
			</div>
		</div>,
		document.querySelector('#modal')
	);
};

export default Modal;
