import React from 'react';
import { Link } from 'react-router-dom';
import ItemList from './items/ItemList';
import ItemsAnalytics from './items/ItemsAnalytics';


const ItemsListing = () => {
	
	return (
		<div>
			<ItemsAnalytics />
			<ItemList />
			<div className='fixed-action-btn'>
				<Link to='/items/create' href="/" className="btn-floating btn-large red">
					<i className="large material-icons">add</i>
				</Link>
			</div>
		</div>
	);	
}

export default ItemsListing;