import React from 'react';
import CurrencyInput from 'react-currency-input-field';
 
export default ({ input, label, meta: { error, touched } }) => (
    <div>
    	<label>{label}</label><br/>
	    <CurrencyInput
	    	prefix={"$"}
			defaultValue={0}
			allowDecimals={true}
			decimalsLimit={2}
			onChange={input.onChange}
		/>
		<div className='red-text' style={{marginBottom:'20px'}}>{touched && error}</div>
	</div>
);

