
import React from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

export default ({ input, label, placeholder, minDate, maxDate, meta: { error, touched } }) => (
    <div>
    	<label>{label}</label><br/>
	    <DatePicker
	        {...input}
			className="plus-icon"
			onChange={date => input.onChange(moment(date).format("DD/MM/YYYY"))}
	    />
	    <div className='red-text' style={{marginBottom:'20px'}}>{touched && error}</div>
	</div>
);
