import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createItem } from '../../actions';
import ItemForm from './ItemForm';

class ItemCreate extends Component {
	onSubmit = formValues => {
		this.props.createItem(formValues);
	}

	render() {
		return (
			<div>
				<h4>Create New Item</h4>
				<ItemForm onSubmit={this.onSubmit}/>
			</div>
		);
	}
}

export default connect(null, {createItem})(ItemCreate);