import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchSingleItem, deleteItem } from '../../actions';
import Modal from '../Modal';
import history from '../../history';

class ItemDelete extends Component {
	componentDidMount() {
		this.props.fetchSingleItem(this.props.match.params.id);
	}

	renderContent() {
		if (!this.props.item) {
			return 'Are you sure you want to delete this item?';
		}

		return `Are you sure you want to delete ${this.props.item.name}?`;
	}

	renderActions() {
		const { id } = this.props.match.params;
		return (
			<React.Fragment>
				<Link to='/' className="grey btn-flat right white-text">
					Cancel
				</Link>
				<button
					onClick={() => this.props.deleteItem(id)}
					className="red btn-flat white-text"
					style={{ margin: '6px' }}
				>
					Delete
				</button>
			</React.Fragment>
		);
	}

	render() {
		return (
			<Modal
				title="Delete Item"
				content={this.renderContent()}
				actions={this.renderActions()}
				onDismiss={() => {
					history.push('/');
				}}
			/>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		item: state.items[ownProps.match.params.id],
	};
};

export default connect(mapStateToProps, { fetchSingleItem, deleteItem })(
	ItemDelete
);
