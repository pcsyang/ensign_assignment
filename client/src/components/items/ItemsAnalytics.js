import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchItems } from '../../actions';
import Chart from './ItemLineChart';

class ItemsAnalytics extends Component {
	componentDidMount() {
		this.props.fetchItems();
	}

	renderChart() {
		if(!this.props.items) {
			return (<div>Loading...</div>);
		}
		return (
			<div><Chart data={this.props.items}/></div>
		);
	}

	render() {
		return (
			<div>
				<h3>Items Analytics</h3>
				{this.renderChart()}
			</div>
		);
	}
}

function mapStateToProps(state) {
	// console.log("state in ItemsAnalytics: ", state);
	return {
		items: Object.values(state.items),
	};
}

export default connect(mapStateToProps, { fetchItems })(ItemsAnalytics);
