//Item form that shows fields for user input
import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router-dom';
import ItemField from './ItemField';
import CurrencyInput from './CurrencyInput';
import FieldDatePicker from './FieldDatePicker';
import formFields from './formFields';

class ItemForm extends Component {

	renderFields() { 
		let all_fields = formFields.map(({ label, name, type, component }) => {
			if(component === "ItemField") {
				return (
					<Field
						key={name}
						component={ItemField}
						type={type}
						name={name}
						label={label}
					/>
				);
			}
			else if(component === "FieldDatePicker") {
				return (
					<Field
						key={name}
						component={FieldDatePicker}
						type={type}
						name={name}
						label={label}
    					placeholder="DD/MM/YYYY"
					/>
				);
			}
			else { //CurrencyInput
				return (
					<Field
						key={name}
						component={CurrencyInput}
						type={type}
						name={name}
						label={label}
					/>
				);
			}
		});

		return all_fields;
	}

	render() {
		return (
			<div>
				<form onSubmit={this.props.handleSubmit(this.props.onSubmit)}>
					{this.renderFields()}
					<Link
						className="grey btn-flat left white-text"
						to="/"
					>
						Cancel
					</Link>
					<button
						type="submit"
						className="teal btn-flat right white-text"
					>
						Submit
						<i className="material-icons right">done</i>
					</button>
				</form>
			</div>
		);
	}
}

function validate(values) {
	const errors = {};

	// console.log("values: ", values);

	//Check for empty fields
	formFields.forEach(({ name }) => {

		if (!values[name]) {
			errors[name] = 'Please provide a value';
		}

		if(name === "quantity") {
			// console.log("parseInt(values[name]): ", parseInt(values[name]));
			const checkQuant = parseInt(values[name])
			if(checkQuant < 0 || isNaN(checkQuant) || values[name]%1 !== 0) {
				errors[name] = 'Please key in whole number format and quantity greater than 0';
			}
			
		}
		
	});


	return errors;
}

export default reduxForm({
	validate,
	form: 'itemForm'
})(ItemForm);
