//Item form that shows fields for user input
import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { Link } from 'react-router-dom';
import ItemField from './ItemField';
import CurrencyInput from './CurrencyInput';
import FieldDatePicker from './FieldDatePicker';
import formFields from './formFields';

class ItemUpdateForm extends Component {

	renderFields() { 
		let all_fields = formFields.map(({ label, name, type, component }) => {
			if(component === "ItemField") {
				return (
					<Field
						key={name}
						component={ItemField}
						type={type}
						name={name}
						label={label}
					/>
				);
			}
			else if(component === "FieldDatePicker") {
				return (
					<Field
						key={name}
						component={FieldDatePicker}
						type={type}
						name={name}
						label={label}
    					placeholder="DD/MM/YYYY"
					/>
				);
			}
			else { //CurrencyInput
				return (
					<Field
						key={name}
						component={CurrencyInput}
						type={type}
						name={name}
						label={label}
					/>
				);
			}
		});

		return all_fields;
	}

	render() {
		return (
			<div>
				<form onSubmit={this.props.handleSubmit(this.props.onItemSubmit)}>
					{this.renderFields()}
					<Link
						className="red btn-flat left white-text"
						to="/"
					>
						Cancel
					</Link>
					<button
						type="submit"
						className="teal btn-flat right white-text"
					>
						Next
						<i className="material-icons right">done</i>
					</button>
				</form>
			</div>
		);
	}
}

function validate(values) {
	const errors = {};

	console.log("values: ", values);

	//Check for empty fields
	formFields.forEach(({ name }) => {
		if (!values[name]) {
			errors[name] = 'Please provide a value';
		}
		if(name === "quantity" && !parseInt(values[name]))
			errors[name] = 'Please only key in numbers';
	});


	return errors;
}

export default reduxForm({
	validate,
	form: 'itemUpdateForm',
	destroyOnUnmount: false
})(ItemForm);
