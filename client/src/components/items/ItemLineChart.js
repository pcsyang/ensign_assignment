import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

class Chart extends React.Component {

    render() {

        // console.log("LineChart props: ", this.props);

        const raw_data = this.props.data

        var group_by_date = [];

        for(let i=0; i<raw_data.length; i++) {

            const { price, quantity, dateUpdated } = raw_data[i];

            let exist = false;

            group_by_date.find((entry, i) => {
                if (entry.date === dateUpdated) {
                    group_by_date[i].total_quantity += quantity;
                    group_by_date[i].total_items += 1;
                    group_by_date[i].total_price += (price*quantity);
                    exist = true; 
                    return true;// stop searching
                }
            });

            if(!exist) {
                group_by_date.push({
                    date: dateUpdated,
                    total_quantity: quantity,
                    total_price: (price*quantity),
                    total_items: 1,
                });
            }
        }

        // console.log("group_by_date: ", group_by_date);

        var xAxis_data = [];
        var avg_price_data = [];
        var avg_quantity_data = [];

        group_by_date.sort(function(a,b){
            return new Date(a.date) - new Date(b.date);
        });

        group_by_date.map(entry => {
        	xAxis_data.push(new Date(entry.date).toLocaleDateString());
        	avg_price_data.push(parseFloat((entry.total_price/entry.total_quantity).toFixed(2)));
            avg_quantity_data.push({ 
                y: parseFloat((entry.total_quantity/entry.total_items).toFixed(2)),
                total_quantity: entry.total_quantity,
                total_price: (entry.total_price).toFixed(2),
                total_items: entry.total_items
            });
        });

        // console.log("avg_price_data: ", avg_price_data);
        // console.log("avg_quantity_data: ", avg_quantity_data);

        const options = {
		  chart: {
		  },
		  title: {
		    text: 'Items Statistics'
		  },
		  xAxis: [{
		    categories: xAxis_data,
		    crosshair: true
		  }],
		  yAxis: [{ // Primary yAxis
		    title: {
		      text: 'Average Price',
		      style: {
		        color: Highcharts.getOptions().colors[0]
		      }
		    },
		    labels: {
		      style: {
		        color: Highcharts.getOptions().colors[0]
		      }
		    }
		  }, { // Secondary yAxis
		    title: {
		      text: 'Average Quantity',
		      style: {
		        color: Highcharts.getOptions().colors[1]
		      }
		    },
		    labels: {
		      style: {
		        color: Highcharts.getOptions().colors[1]
		      }
		    },
		    opposite: true
		  }],
		  tooltip: {
		    shared: true,
		    formatter: function() {
		       var text = '';
		       // console.log("this.points[1]: ", this.points[1]);
				if (this.points[0].series.name === 'Average Price') {
					text += '<b>Date: </b>' + this.points[0].x + '<br>' + 
					'<br> <b>'+ this.points[0].series.name + ': $</b>' + this.points[0].y;
				}
				if (this.points[1].series.name === 'Average Quantity') {
					text += '<br> <b>' + this.points[1].series.name + ': </b>' + this.points[1].y + 
					'<br><br> <b>Total Quantity: </b>'+ this.points[1].point.total_quantity + 
					'<br> <b>Total Price: $</b>'+ this.points[1].point.total_price + 
					'<br> <b>Total Items Type: </b>' + this.points[1].point.total_items;
				}	
		       
		       return text;
		     }
		  },
		  series: [{
		    name: 'Average Price',
		    type: 'line',
		    yAxis: 1,
		    data: avg_price_data,
		    events: {
	        		legendItemClick: function(e) {
	            		e.preventDefault()
	            }
	        }
		  }, {
		    name: 'Average Quantity',
		    type: 'line',
		    data: avg_quantity_data,
		    events: {
	        		legendItemClick: function(e) {
	            		e.preventDefault()
	            }
	        }
		  }],
		  credits: {
		    enabled: false
		  },
		  exporting: {
		    enabled: false
		  }
		};

        if(raw_data.length > 0) {
            return (
				<div>
					<HighchartsReact highcharts={Highcharts} options={options} />
				</div>
            );
        }
        else {
            return (<div><p>No Data to plot. Add some items!</p></div>);
        }
    }
	
    
}
export default Chart;