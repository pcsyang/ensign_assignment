import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchSingleItem, updateItem } from '../../actions';
import ItemForm from './ItemForm';
import moment from 'moment';

class ItemUpdate extends Component {
	componentDidMount() {
		// console.log("this.props.match.params.id: ", this.props.match.params.id);
		this.props.fetchSingleItem(this.props.match.params.id);
		// console.log("updateItem, this.props: ", this.props);
	}

	onSubmit = (formValues) => {
		// console.log("formValues before update: ", formValues);
		this.props.updateItem(this.props.match.params.id, formValues);
	}

	render() {
		// console.log(this.props);

		if(!this.props.item) {
			return <div>Loading...</div>
		}

		const { name, quantity, price } = this.props.item;
		let dateCreated = moment(this.props.item.dateCreated).format("DD/MM/YYYY")
		return (<div>
					<h4>Update Item</h4>
					<ItemForm 
						initialValues={ {name, quantity, price, dateCreated} }
						onSubmit={this.onSubmit}/>
				</div>
		);	
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		item: state.items[ownProps.match.params.id]
	}
}

export default connect(mapStateToProps, { fetchSingleItem, updateItem })(ItemUpdate);