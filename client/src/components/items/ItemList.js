import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchItems } from '../../actions';

class ItemList extends Component {
	componentDidMount() {
		this.props.fetchItems();
	}

	renderItems() {

		if(!this.props.items) {
			return (<div>Loading...</div>);
		}
		return this.props.items.map(item => {
			return (
				<div className='card darken-1' key={item._id}>
					<div className='card-content'>
						<Link to={`/items/delete/${item._id}`} className="red btn-flat right white-text">
							Delete
						</Link>
						<Link to={`/items/update/${item._id}`} className="teal btn-flat right white-text">
							Edit
						</Link>
						<Link to={`/items/details/${item._id}`} className="teal btn-flat right white-text">
							Details
						</Link>
						<span className='card-title'>{item.name}</span>
						<p>Quantity: {item.quantity}</p>
						<p>Price: ${item.price}</p>
						<p className='right'>Last Updated: {new Date(item.dateUpdated).toLocaleDateString()}</p>
						
					</div>
				</div>
			);
		});
	}

	render() {
		return (
			<div>
				<h3>Items List</h3>
				{this.renderItems()}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		items: Object.values(state.items),
	};
}

export default connect(mapStateToProps, { fetchItems })(ItemList);
