import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchSingleItem } from '../../actions';

class ItemDetails extends Component {
	componentDidMount() {
		// console.log("this.props.match.params.id: ", this.props.match.params.id);
		this.props.fetchSingleItem(this.props.match.params.id);
	}

	renderHistory(item) {
		const history = item.history;
		// console.log("history: ", history)

		return history.map(entry => {
			return (
				<tr key={entry._id}>
					<td>{entry.name}</td>
					<td>{entry.quantity}</td>
					<td>${entry.price}</td>
					<td>{new Date(entry.date).toLocaleDateString()}</td>
				</tr>
			);
		})
	}

	render() {
		// console.log(this.props);

		if(!this.props.item) {
			return <div>Loading...</div>
		}

		const item = this.props.item;
		return (<div>
					<h5>Item Details</h5>
					<table>
						<thead>
							<tr>
								<th>Name</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>First Created</th>
								<th>Last Updated</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{item.name}</td>
								<td>{item.quantity}</td>
								<td>${item.price}</td>
								<td>{new Date(item.dateCreated).toLocaleDateString()}</td>
								<td>{new Date(item.dateUpdated).toLocaleDateString()}</td>
							</tr>
						</tbody>
					</table>
					
					<h5>Update History</h5>
					<table>
						<thead>
							<tr>
								<th>Name</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							{this.renderHistory(item)}
						</tbody>
					</table>
					<Link
						className="teal btn-flat left white-text"
						to="/"
					>
						Back to listing
					</Link>
				</div>
		);	
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		item: state.items[ownProps.match.params.id]
	}
}

export default connect(mapStateToProps, { fetchSingleItem })(ItemDetails);