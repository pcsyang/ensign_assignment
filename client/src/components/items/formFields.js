export default [
	{ label: 'Item name', name: 'name', type: 'text', component: 'ItemField' },
	{ label: 'Quantity', name: 'quantity', type: 'number', component: 'ItemField' },
	{ label: 'Price', name: 'price', type: 'currency', component: 'CurrencyInput' },
	{ label: 'Date', name: 'dateCreated', type: 'date', component: 'FieldDatePicker'}
];