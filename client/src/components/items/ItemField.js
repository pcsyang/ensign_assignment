//Contains the logic to handle a single label and text input field

import React from 'react';

export default ({ input, label, meta: { error, touched } }) => {
	
	return (
		<div>
			<label>{label}</label>
			<input {...input}/>
			<div className='red-text' style={{marginBottom:'20px'}}>{touched && error}</div>
		</div>
	);
};

//using {...input} is equivilent to typing onBlur:{input.onBlur}, onChange:{input.onChange} etc