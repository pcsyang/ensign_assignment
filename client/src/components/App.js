import React, { Component } from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux'; 
import * as actions from '../actions';
import history from '../history';


import Header from './Header';
import ItemsListing from './ItemsListing';
import ItemCreate from './items/ItemCreate';
import ItemUpdate from './items/ItemUpdate';
import ItemDelete from './items/ItemDelete';
import ItemDetails from './items/ItemDetails'; 

class App extends Component {
	componentDidMount() {
		this.props.fetchItems();
	}

	render() {
		return (
			<div className="container">
				<Router history={history}>
					<div>
						<Header />
						<Route exact path="/" component={ItemsListing} />
						<Route exact path="/items/create" component={ItemCreate} />
						<Route exact path="/items/update/:id" component={ItemUpdate} />
						<Route exact path="/items/delete/:id" component={ItemDelete} />
						<Route exact path="/items/details/:id" component={ItemDetails} />
					</div>
				</Router>
			</div>
		);
	}
}

export default connect(null, actions)(App);
