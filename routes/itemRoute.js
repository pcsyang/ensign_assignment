const mongoose = require('mongoose');
const Item = mongoose.model('items');
const ItemHistory = require('../models/ItemHistory');
const { Path } = require('path-parser');
const { URL } = require('url');

module.exports = (app) => {
	//Get all items
	app.get('/api/items', async (req, res) => {
		const items = await Item.find();

		res.send(items);
	});

	//Create item
	app.post('/api/item/create', async (req, res) => {
		const { id, name, quantity, price, dateCreated } = req.body;

		console.log("dateCreated: ", dateCreated);

		try {

			let dateArray = dateCreated.split("/")

			const item = await new Item({
				name,
				quantity,
				price,
				dateCreated: new Date(dateArray[2], dateArray[1]-1, dateArray[0]).toLocaleDateString(),
				dateUpdated: new Date(dateArray[2], dateArray[1]-1, dateArray[0]).toLocaleDateString()
			}).save();
			
			console.log("item: ", item);

			// const items = await Item.find();
			res.send(item);

		} catch (err) {
			res.status(500).send(err);
		}
		
	});

	//Get single item
	app.get('/api/item/:id', async (req, res) => {
		const { id } = req.params;

		try {
			const item = await Item.findById(id);

			res.send(item);
		} catch (err) {
			res.status(500).send(err);
		}
		
	});

	//Update item
	app.patch('/api/item/:id', async (req, res) => {
		try {
			const { id } = req.params;
			const { name, quantity, price, dateCreated } = req.body;

			console.log("Inside update api....", req.body);

			let item = await Item.findById(id);

			console.log("item retrieved: ", item);

			const itemHistory = {
				name: item.name,
				quantity: item.quantity,
				price: item.price,
				date: item.dateUpdated
			};

			item.history.push(itemHistory)
			item.name = name;
			item.quantity = quantity;
			item.price = price;

			let dateArray = dateCreated.split("/")
			item.dateUpdated = new Date(dateArray[2], dateArray[1]-1, dateArray[0]).toLocaleDateString();

			const updatedItem = await item.save();

			// console.log("item updated? : ", updatedItem);
			res.send(updatedItem);
		} catch (err) {
			console.log(err);
			res.status(500).send(err);
		}
	});

	//Delete item
	app.delete('/api/item/:id', async (req, res) => {
		const { id } = req.params;

		try {
			// console.log("Deleting ", id);
			const result = await Item.findByIdAndDelete(id);	

			// console.log("Delete result:", result);
			res.send(result);
		} catch (err) {
			res.status(500).send(err);
		}
	});
}